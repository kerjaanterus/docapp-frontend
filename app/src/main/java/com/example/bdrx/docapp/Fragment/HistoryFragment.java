package com.example.bdrx.docapp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bdrx.docapp.R;


/**
 * Created by reksa on 07/06/17.
 */

public class HistoryFragment extends Fragment {


    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_history, container, false);
        return mView;
    }

    public static Fragment newInstance() {
        return new HistoryFragment();
    }

}
